#ifndef DATA_MANAGE_H_
#define DATA_MANAGE_H_
#include "stdint.h"
#define BUFFER_SIZE 		128
typedef struct
{
	uint8_t *w_buf;
	uint8_t *r_buf;
	uint16_t buf_full_num;
	uint16_t buf_w_num;
	uint16_t buf_r_num;
	uint16_t buf_r_index;
	uint16_t buf_w_index;
	uint32_t eprom_addr;
	uint32_t eprom_len;
}data_manage_t;

void eprom_init(void);


void data_manage_init(data_manage_t * p_dm,uint8_t* buf,uint32_t length);

void data_write(data_manage_t * p_dm,uint8_t* data,uint32_t length);

void data_read_index_reset(data_manage_t * p_dm);

void data_read(data_manage_t * p_dm,uint16_t cnt,uint8_t * data);

uint16_t get_data_pkg_cnt(data_manage_t * p_dm);

void data_empty(data_manage_t * p_dm);

#endif
