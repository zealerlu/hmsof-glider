#ifndef PROTOCOL_H_
#define PROTOCOL_H_
#include <stdint.h>

#define CMD_CLEAN_ROUND			0x80
#define CMD_CLEAN_BUFF			0x81
#define CMD_SET_TIME			0x82
#define CMD_SET_FEETER			0x83

#define CMD_GET_COUND_AVAGYRO	0x10

#define CMD_GET_TEMP_PKG_CNT	0x11
#define CMD_GET_TEMP_PKG		0x01

#define CMD_GET_LIGHT_PKG_CNT	0x12
#define CMD_GET_LIGHT_PKG		0x02

#define CMD_GET_NOISE_PKG_CNT	0x13
#define CMD_GET_NOISE_PKG		0x03

#define CMD_GET_ACT_PKG_CNT		0x14
#define CMD_GET_ACT_PKG			0x04



void BLE_send(uint8_t *data, uint8_t len);
uint8_t recv_protocol(uint8_t * data,uint8_t length);

#endif
