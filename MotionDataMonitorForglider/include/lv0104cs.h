//! \file lv0104cs.h
//!
//! \mainpage
//!
//! This driver library contains the ON Semiconductor's <b>lv0104cs
//! Ambient Light Sensor</b> API
//!
//! Product page:<br>
//! https://www.onsemi.com/PowerSolutions/product.do?id=LV0104CS
//!
//! IDK Evaluation board:<br>
//! https://www.onsemi.com/PowerSolutions/evalBoard.do?id=ALS-GEVB
//!
//! API reference:<br>
//! \ref LV0104CS_DRIVER_GRP
//!
//! \addtogroup LV0104CS_DRIVER_GRP LV0104CS API
//! \{
//-----------------------------------------------------------------------------
#ifndef LV0104CS_H_
#define LV0104CS_H_

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

//! \name Library error codes
//! \{

/** \brief Library call was successful. */
#define LV0104CS_OK                     (0)

/** \brief Invalid pointer argument was passed to library function. */
#define LV0104CS_E_NULL_PTR             (1)

/** \brief Transfer function indicated communication error. */
#define LV0104CS_E_COMM                 (2)

/** \brief Device ID register content does not match expected value. */
#define LV0104CS_E_DEV_NOT_FOUND        (3)

/** \brief Argument passed to library function has invalid value. */
#define LV0104CS_E_OUT_OF_RANGE         (4)

/** \brief Given operation is not supported. */
#define LV0104CS_E_UNSUPPORTED          (5)

/** \brief Success return code for I2C transfer function. */
#define LV0104CS_COMM_OK                (0)

/** \brief Failure return code for I2C transfer function. */
#define LV0104CS_COMM_ERROR             (1)

//! \}

//============================================================================//
//LV0104CS Slave Address:D6     D5     D4     D3     D2     D1     D0    R/W
//                       0      0      1      0      0      1      1     R=1,W=0
/** \brief LV0104CS I2C Interface 7-bit address */
#define LV0104CS_I2C_ADDR               (0x13)


//============================================================================//
//Control Register(Measurement):D7     D6     D5     D4     D3     D2     D1     D0
//                              MODE1  MODE0  -      GAIN1  GAIN0  INTEG1 INTEG0 MANUAL(Start(0)/Stop(1))

//MODE[1:0] :  11       Active
//             00       Sleep
#define LV0104CS_MODE_DEFAULT			(0x2C)

#define LV0104CS_POWER_MASK             (0xC0)
#define LV0104CS_POWER_POSE             (0x06)
/** \brief 传感器睡眠模式*/
#define LV0104CS_POWER_DOWN             (0x00)
/** \brief 传感器工作模式*/
#define LV0104CS_POWER_ON               (0x03)
/** \brief 传感器设置模式*/
#define LV0104CS_SETTING                (0x02)


#define LV0104CS_ENABLE_MASK             (0x01)
#define LV0104CS_ENABLE_POSE             (0x00)
/** \brief 传感器启动*/
#define LV0104CS_ENABLE             	(0x00)
/** \brief 传感器停止*/
#define LV0104CS_DISABLE                (0x01)

//Integration INTEG[1:0] :  11  Manual
//                          10  200mS
//                          01  100mS
//                          00  12.5mS
#define LV0104CS_INTEG_TIME_MASK        (0x06)
#define LV0104CS_INTEG_TIME_POSE        (0x01)

#define LV0104CS_INTEG_TIME_MANUAL      (0x06)
#define LV0104CS_INTEG_TIME_200MS       (0x04)
#define LV0104CS_INTEG_TIME_100MS       (0x02)
#define LV0104CS_INTEG_TIME_12p5MS      (0x00)



//GAIN[1:0] :  11       X8
//             10       X2
//             01       X1
//             00       X0.25
#define LV0104CS_GAIN_MASK        		(0x18)
#define LV0104CS_GAIN_POSE        		(0x03)

#define LV0104CS_GAIN_x8      			(0x18)
#define LV0104CS_GAIN_x2       			(0x10)
#define LV0104CS_GAIN_x1       			(0x08)
#define LV0104CS_GAIN_x0p25      		(0x00)



/** \brief Control structure for LV0104CS containing platform dependent
 * communication functions and device parameters.
 *
 * \note
 * All member variables of this structure have to be set before it can be used
 * with the LV0104CS API functions.
 */
struct lv0104cs_t
{
	int32_t (*read_func)(uint8_t dev_id, uint8_t *value);
	int32_t (*write_func)(uint8_t dev_id, uint8_t value);
	void (*delay_func)(uint32_t ms);
	/** \brief setting cmd */
	uint8_t cmd;

	uint8_t sensitivity;

	/** \brief Device id passed to bus read / write functions.
	 *
	 * Needs to be set before calling lv0104cs_init function.
	 */
	uint8_t id;
};

/** \brief Initializes LV0104CS Ambient Light Sensor.
 *
 * This function does the following steps:
 *
 * 1. Issues Software reset command to LV0104CS.
 * 2. Tests for correct Device ID value.
 * 3. Turns the sensor into power off mode.
 *
 * \param dev
 * Pointer to LV0104CS control structure.
 * This structure must have all of its member variables set to platform specific
 * communication functions before calling this function.
 *
 * \returns
 * \ref LV0104CS_OK - On success.<br>
 * \ref LV0104CS_E_DEV_NOT_FOUND - Device ID mismatch.<br>
 * \ref LV0104CS_E_NULL_PTR - \p dev pointer or its value are invalid.
 * \b LV0104CS_E_* - Error code on error.
 */
extern int32_t lv0104cs_init(struct lv0104cs_t *dev);

/** \brief Set LV0104CS mode.
 *
 * \param power_mode
 *	LV0104CS_POWER_DOWN 睡眠模式
	LV0104CS_POWER_ON 活跃模式
	LV0104CS_SETTING 设置模式
 * \param dev
 * Pointer to initialized LV0104CS control structure.
 *
 * \returns
 * \ref LV0104CS_OK - On success.<br>
 * \b LV0104CS_E_* - Error code on error.
 */
extern int32_t lv0104cs_set_mode(uint8_t power_mode, struct lv0104cs_t *dev);

/* 
	获取 lv0104cs 工作模式
	LV0104CS_POWER_DOWN 睡眠模式
	LV0104CS_POWER_ON 活跃模式
	LV0104CS_SETTING 设置模式
*/
extern int32_t lv0104cs_get_mode(uint8_t *power_mode,
		struct lv0104cs_t *dev);
/* 
	设置 lv0104cs 使能
	LV0104CS_ENABLE  传感器使能
	LV0104CS_DISABLE 传感器失能
*/
extern int32_t lv0104cs_set_enable(uint8_t enable, struct lv0104cs_t *dev);
/* 
	获取 lv0104cs 使能
	LV0104CS_ENABLE  传感器使能
	LV0104CS_DISABLE 传感器失能
*/
extern int32_t lv0104cs_get_enable(uint8_t *enable, struct lv0104cs_t *dev);
/* 
	设置 lv0104cs 积分时间
	LV0104CS_INTEG_TIME_MANUAL	手动模式，根据读取时间计算
	LV0104CS_INTEG_TIME_200MS	200ms
	LV0104CS_INTEG_TIME_100MS	100ms
	LV0104CS_INTEG_TIME_12p5MS	12.5ms
*/
extern int32_t lv0104cs_set_integration_time(uint8_t integ_time,
		struct lv0104cs_t *dev);
/* 
	获取 lv0104cs 积分时间
	LV0104CS_INTEG_TIME_MANUAL	手动模式，根据读取时间计算
	LV0104CS_INTEG_TIME_200MS	200ms
	LV0104CS_INTEG_TIME_100MS	100ms
	LV0104CS_INTEG_TIME_12p5MS	12.5ms
*/
extern int32_t lv0104cs_get_integration_time(uint8_t *integ_time,
		struct lv0104cs_t *dev);
/* 
	设置 lv0104cs 增益参数
	LV0104CS_GAIN_x8	8倍
	LV0104CS_GAIN_x2	2倍
	LV0104CS_GAIN_x1	1倍
	LV0104CS_GAIN_x0p25	0.25倍
*/
int32_t lv0104cs_set_gain(uint16_t gain,
		struct lv0104cs_t *dev);
/* 
	获取 lv0104cs 增益参数
	LV0104CS_GAIN_x8	8倍
	LV0104CS_GAIN_x2	2倍
	LV0104CS_GAIN_x1	1倍
	LV0104CS_GAIN_x0p25	0.25倍
*/
int32_t lv0104cs_get_gain(uint16_t *gain,
		struct lv0104cs_t *dev);
/* 
	设置 lv0104cs 灵敏度参数
	sensitivity 范围 0x00-0x3f
	Sensitivity Register :D7     D6     D5     D4     D3     D2     D1     D0
						MODE1  MODE0  ADJ5   ADJ4   ADJ3   ADJ2   ADJ1   ADJ0
	MODE[1:0] :  10       Setting
	ADJ5  :       1       Plus
				  0       Minus
	ADJ[4:0]  :   ADJ[5]=0     2*ADJ[4:0] / (2*ADJ[4:0] + 1)
				  ADJ[5]=1    (2*ADJ[4:0] + 1) / 2*ADJ[4:0]
*/

int32_t lv0104cs_set_sensitivity(uint16_t sensitivity,
		struct lv0104cs_t *dev);
/* 
	获取 lv0104cs 灵敏度参数
	sensitivity 范围 0x00-0x3f
	Sensitivity Register :D7     D6     D5     D4     D3     D2     D1     D0
						MODE1  MODE0  ADJ5   ADJ4   ADJ3   ADJ2   ADJ1   ADJ0
	MODE[1:0] :  10       Setting
	ADJ5  :       1       Plus
				  0       Minus
	ADJ[4:0]  :   ADJ[5]=0     2*ADJ[4:0] / (2*ADJ[4:0] + 1)
				  ADJ[5]=1    (2*ADJ[4:0] + 1) / 2*ADJ[4:0]
*/
int32_t lv0104cs_get_sensitivity(uint16_t *sensitivity,
		struct lv0104cs_t *dev);
/* 
	读取 lv0104cs 数据
*/
//============================================================================//
//Measuration Result Register(READ) :DH7  DH6  DH5  DH4  DH3  DH2  DH1  DH0         Date Higher Byte
//                                   DL7  DL6  DL5  DL4  DL3  DL2  DL1  DL0         Date Lower Byte
//============================================================================//

int32_t lv0104cs_read_lux_data(uint32_t *lux_data,
		struct lv0104cs_t *dev);

#ifdef __cplusplus
}
#endif

#endif /* LV0104CS_H_ */

//! \}
