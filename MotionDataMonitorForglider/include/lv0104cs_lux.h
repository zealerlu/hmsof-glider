#ifndef LV0104CS_LUX_H_
#define LV0104CS_LUX_H_

#include <stdbool.h>
#include <stdint.h>

// Platform independent library for reading / writing of LV0104CS I2C registers.


#ifdef __cplusplus
extern "C"
{
#endif

#include <lv0104cs.h>
/** \brief Callback function prototype used to report light level to
 * application.
 *
 * \param lux
 * Latest measured ambient light value measured by sensor.
 */
typedef void(*LV0104CS_LUX_ReadCallback)(uint32_t lux);

/** \brief Initializes LV0104CS and puts it into power down mode.
 *
 * \param I_k
 * Integration constant used for conversion of sensor tick count to LUX.
 * Datasheet specifies value of 7.7 (I_k = 7700) as integration constant for
 * white LED light source.<br>
 * Value has to be multiplied by scale factor of 1000
 * (LV0104CS_INTEG_CONSTANT_DIV) to allow for integer based computation.
 *
 * \param T_i
 * Integration time setting to be used by sensor.
 * Use one of <b>LV0104CS_INTEG_TIME_[ 800MS | 400MS | 200MS | 100MS | 50MS |
 * 25MS | 12p5MS | 6p25MS]</b>
 *
 * \returns
 * \ref LV0104CS_OK - On success.<br>
 * \b LV0104CS_E_* - Error code on failure.
 *
 */
extern int32_t LV0104CS_LUX_InitializeParam(uint32_t sensitivity, uint8_t T_i);

/** \brief Initializes LV0104CS and puts it into power down mode.
 *
 * This function uses predefined sensor parameters.
 *
 * \see LV0104CS_LUX_InitializeParam
 */
static inline int32_t LV0104CS_LUX_Initialize(void)
{
	return LV0104CS_LUX_InitializeParam(0x3f, LV0104CS_INTEG_TIME_200MS);
}

/** \brief Powers on the sensor for enough time to get single measurement and
 * calls given callback function.
 *
 * The callback is called after 4 integration cycles have passed to allow for
 * sensor output to stabilize.
 *
 * Sensor will be put back to power down state after measurement is complete.
 *
 * \param cb
 * Optional callback to be called when measurement is completed.
 *
 * \returns
 * \ref LV0104CS_OK - On success.<br>
 * \b LV0104CS_E_* - Error code on failure.
 */

extern int32_t LV0104CS_LUX_StartContinuous(uint32_t period_ms, LV0104CS_LUX_ReadCallback cb);

/** \brief Powers off the sensor and disables periodic and interrupt callbacks.
 *
 * \returns
 * \ref LV0104CS_OK - On success.<br>
 * \b LV0104CS_E_* - Error code on failure.
 */
extern int32_t LV0104CS_LUX_Stop(void);

/** \brief Reads latest measured lux value from sensor.
 *
 * \param lux
 * Pointer to variable where to store read lux value.
 *
 * \returns
 * \ref LV0104CS_OK - On success.<br>
 * \b LV0104CS_E_* - Error code on failure.
 */
extern int32_t LV0104CS_LUX_ReadLux(uint32_t *lux);


#ifdef __cplusplus
}
#endif

#endif /* LV0104CS_LUX_H_ */

//! \}
//! \}
//! \}
