#include <lv0104cs_lux.h>
#include <lv0104cs.h>
#include <BDK.h>

#include <EventCallback.h>
#include <SoftwareTimer.h>



static struct lv0104cs_t lv0104_dev;

static LV0104CS_LUX_ReadCallback lv0104_read_cb = NULL;

static struct SwTimer lv0104_timer;

/* 
    连续读取定时器回调函数 
*/
static void LV0104CS_LUX_ContinuousTimerCallback(void *arg)
{
    SwTimer_Advance(&lv0104_timer);

    if (lv0104_read_cb != NULL)
    {
        uint32_t lux = 0;
        lv0104cs_read_lux_data(&lux, &lv0104_dev);
        lv0104_read_cb(lux);
    }
}
/* 
    iic接口
 */
static int32_t LV0104CS_LUX_BusRead(uint8_t dev_id, uint8_t *value)
{

    if (HAL_I2C_Read(dev_id, value, 2, false) != HAL_OK)
    {
        return LV0104CS_COMM_ERROR;
    }

    return LV0104CS_COMM_OK;
}
/* 
    iic接口
 */
static int32_t LV0104CS_LUX_BusWrite(uint8_t dev_id, uint8_t value)
{

    if (HAL_I2C_Write(dev_id, &value, 1, false) != HAL_OK)
    {
        return LV0104CS_COMM_ERROR;
    }

    return LV0104CS_COMM_OK;
}
/* 
    LV0104CS初始化函数
 */
int32_t LV0104CS_LUX_InitializeParam(uint32_t sensitivity, uint8_t T_i)
{
    int32_t retval = LV0104CS_OK;

    lv0104_dev.id = LV0104CS_I2C_ADDR;
    lv0104_dev.sensitivity = sensitivity;

    lv0104_dev.read_func = &LV0104CS_LUX_BusRead;
    lv0104_dev.write_func = &LV0104CS_LUX_BusWrite;
    lv0104_dev.delay_func = &HAL_Delay;

    retval = lv0104cs_init(&lv0104_dev);
    if (retval != LV0104CS_OK)
    {
        return retval;
    }
    retval = lv0104cs_set_mode(LV0104CS_POWER_ON, &lv0104_dev);
    if (retval != LV0104CS_OK)
    {
        return retval;
    }
    retval = lv0104cs_set_integration_time(T_i, &lv0104_dev);
    if (retval != LV0104CS_OK)
    {
        return retval;
    }

    return retval;
}

/* 
    LV0104CS 开始连续读取
    period_ms 定时周期
    cb 读取回调函数
 */
int32_t LV0104CS_LUX_StartContinuous(uint32_t period_ms,
        LV0104CS_LUX_ReadCallback cb)
{
    int32_t retval = LV0104CS_OK;

    retval = lv0104cs_set_mode(LV0104CS_POWER_ON, &lv0104_dev);
    if (retval == LV0104CS_OK)
    {
        if (cb != NULL)
        {
            lv0104_read_cb = cb;

            if (period_ms != 0)
            {
                SwTimer_Initialize(&lv0104_timer);
                SwTimer_AttachScheduled(&lv0104_timer,
                        &LV0104CS_LUX_ContinuousTimerCallback, NULL);
                SwTimer_ExpireInMs(&lv0104_timer, period_ms);
            }
        }
    }

    return retval;
}

/* 
    LV0104CS 停止传感器
 */
int32_t LV0104CS_LUX_Stop(void)
{
    int32_t retval = LV0104CS_OK;

    retval = lv0104cs_set_mode(LV0104CS_POWER_DOWN, &lv0104_dev);

    // disable timers

    return retval;
}
/* 
    LV0104CS 读取数据
 */
int32_t LV0104CS_LUX_ReadLux(uint32_t *lux)
{
    int32_t retval = LV0104CS_OK;

    uint8_t pm;
    retval = lv0104cs_get_mode(&pm, &lv0104_dev);
    ASSERT_DEBUG(pm == LV0104CS_POWER_ON);

    if (lux != NULL)
    {
        retval = lv0104cs_read_lux_data(lux, &lv0104_dev);
    }
    else
    {
        retval = LV0104CS_E_NULL_PTR;
    }

    return retval;
}
