//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
//! \file lv0104cs.c
//!
//! \addtogroup LV0104CS_DRIVER_GRP
//! \{
//-----------------------------------------------------------------------------
#include <lv0104cs.h>

#include <stdlib.h>

//============================================================================//
//LV0104CS Slave Address:D6     D5     D4     D3     D2     D1     D0    R/W
//                       0      0      1      0      0      1      1     R=1,W=0
//
//============================================================================//
//Control Register(Measurement):D7     D6     D5     D4     D3     D2     D1     D0
//                              MODE1  MODE0  -      GAIN1  GAIN0  INTEG1 INTEG0 MANUAL(Start(0)/Stop(1))

//MODE[1:0] :  11       Active
//             00       Sleep

//GAIN[1:0] :  11       X8
//             10       X2
//             01       X1
//             00       X0.25

//Integration INTEG[1:0] :  11  Manual
//                          10  200mS
//                          01  100mS
//                          00  12.5mS
//
//Resoluation   GAIN1    Gain0   INTEG1  INTEG0
//      0.125       1        1        1       0
//      0.25        1        1        0       1
//      1           1        1        0       0
//      0.5         1        0        1       0
//      1           1        0        0       1
//      8           1        0        0       0
//      1           0        1        1       0
//      2           0        1        0       1
//      16          0        1        0       0
//      4           0        0        1       0
//      8           0        0        0       1
//      64          0        0        0       0
//============================================================================//



/* 
	初始化 lv0104cs 传感器
 */
int32_t lv0104cs_init(struct lv0104cs_t *dev)
{
	int32_t retval = LV0104CS_OK;

	if (dev != NULL && dev->read_func != NULL && dev->write_func != NULL
			&& dev->delay_func != NULL)
	{
		dev->cmd = LV0104CS_MODE_DEFAULT;
		retval = lv0104cs_set_mode(LV0104CS_POWER_DOWN, dev);
		dev->delay_func(2);
	}
	else
	{
		retval = LV0104CS_E_NULL_PTR;
	}

	return retval;
}

/* 
	设置 lv0104cs 工作模式
	LV0104CS_POWER_DOWN 睡眠模式
	LV0104CS_POWER_ON 活跃模式
	LV0104CS_SETTING 设置模式
*/
int32_t lv0104cs_set_mode(uint8_t power_mode, struct lv0104cs_t *dev)
{
	int32_t retval = LV0104CS_OK;
	if (dev != NULL)
	{
		if (   power_mode == LV0104CS_POWER_DOWN
			|| power_mode == LV0104CS_POWER_ON
			|| power_mode == LV0104CS_SETTING)
		{

			dev->cmd &= ~LV0104CS_POWER_MASK;
			dev->cmd |= power_mode<<LV0104CS_POWER_POSE;

			retval = dev->write_func(dev->id, dev->cmd);

			if (retval != LV0104CS_COMM_OK)
			{
				retval = LV0104CS_E_COMM;
			}
		}
		else
		{
			retval = LV0104CS_E_OUT_OF_RANGE;
		}
	}
	else
	{
		retval = LV0104CS_E_NULL_PTR;
	}

	return retval;
}

/* 
	获取 lv0104cs 工作模式
	LV0104CS_POWER_DOWN 睡眠模式
	LV0104CS_POWER_ON 活跃模式
	LV0104CS_SETTING 设置模式
*/
int32_t lv0104cs_get_mode(uint8_t *power_mode, struct lv0104cs_t *dev)
{
	int32_t retval = LV0104CS_OK;

	if (dev != NULL && power_mode != NULL)
	{
		*power_mode = (dev->cmd & LV0104CS_POWER_MASK)>>LV0104CS_POWER_POSE;
	}
	else
	{
		retval = LV0104CS_E_NULL_PTR;
	}

	return retval;
}

/* 
	设置 lv0104cs 使能
	LV0104CS_ENABLE  传感器使能
	LV0104CS_DISABLE 传感器失能
*/
int32_t lv0104cs_set_enable(uint8_t enable, struct lv0104cs_t *dev)
{
	int32_t retval = LV0104CS_OK;

	if (dev != NULL)
	{
		if (   enable == LV0104CS_ENABLE
			|| enable == LV0104CS_DISABLE)
		{
			dev->cmd &= ~LV0104CS_ENABLE_MASK;
			dev->cmd |= enable << LV0104CS_ENABLE_POSE;

			retval = dev->write_func(dev->id, dev->cmd);
			if (retval != LV0104CS_COMM_OK)
			{
				retval = LV0104CS_E_COMM;
			}
		}
		else
		{
			retval = LV0104CS_E_OUT_OF_RANGE;
		}
	}
	else
	{
		retval = LV0104CS_E_NULL_PTR;
	}

	return retval;
}

/* 
	获取 lv0104cs 使能
	LV0104CS_ENABLE  传感器使能
	LV0104CS_DISABLE 传感器失能
*/
int32_t lv0104cs_get_enable(uint8_t *enable, struct lv0104cs_t *dev)
{
	int32_t retval = LV0104CS_OK;

	if (dev != NULL && enable != NULL)
	{
		*enable = (dev->cmd & LV0104CS_ENABLE_MASK)>>LV0104CS_ENABLE_POSE;
	}
	else
	{
		retval = LV0104CS_E_NULL_PTR;
	}

	return retval;
}

/* 
	设置 lv0104cs 积分时间
	LV0104CS_INTEG_TIME_MANUAL	手动模式，根据读取时间计算
	LV0104CS_INTEG_TIME_200MS	200ms
	LV0104CS_INTEG_TIME_100MS	100ms
	LV0104CS_INTEG_TIME_12p5MS	12.5ms
*/
int32_t lv0104cs_set_integration_time(uint8_t integ_time, struct lv0104cs_t *dev)
{
	int32_t retval = LV0104CS_OK;
	uint8_t mode = LV0104CS_POWER_DOWN;
	if (dev != NULL)
	{
		lv0104cs_get_mode(&mode, dev);
		if(mode != LV0104CS_POWER_ON)
		{
			retval = LV0104CS_E_COMM;
		}
		else
		{
			if (integ_time == LV0104CS_INTEG_TIME_MANUAL
				|| integ_time == LV0104CS_INTEG_TIME_200MS
				|| integ_time == LV0104CS_INTEG_TIME_100MS
				|| integ_time == LV0104CS_INTEG_TIME_12p5MS)
			{
				dev->cmd &= ~LV0104CS_INTEG_TIME_MASK;
				dev->cmd |= integ_time<<LV0104CS_INTEG_TIME_POSE;

				retval = dev->write_func(dev->id, dev->cmd);
				if (retval != LV0104CS_COMM_OK)
				{
					retval = LV0104CS_E_COMM;
				}
			}
			else
			{
				retval = LV0104CS_E_OUT_OF_RANGE;
			}
		}
	}
	else
	{
		retval = LV0104CS_E_NULL_PTR;
	}

	return retval;
}
/* 
	获取 lv0104cs 积分时间
	LV0104CS_INTEG_TIME_MANUAL	手动模式，根据读取时间计算
	LV0104CS_INTEG_TIME_200MS	200ms
	LV0104CS_INTEG_TIME_100MS	100ms
	LV0104CS_INTEG_TIME_12p5MS	12.5ms
*/
int32_t lv0104cs_get_integration_time(uint8_t *integ_time, struct lv0104cs_t *dev)
{
	int32_t retval = LV0104CS_OK;

	if (dev != NULL && integ_time != NULL)
	{
		*integ_time = (dev->cmd & LV0104CS_INTEG_TIME_MASK)>>LV0104CS_INTEG_TIME_POSE;
	}
	else
	{
		retval = LV0104CS_E_NULL_PTR;
	}

	return retval;
}

/* 
	设置 lv0104cs 增益参数
	LV0104CS_GAIN_x8	8倍
	LV0104CS_GAIN_x2	2倍
	LV0104CS_GAIN_x1	1倍
	LV0104CS_GAIN_x0p25	0.25倍
*/
int32_t lv0104cs_set_gain(uint16_t gain, struct lv0104cs_t *dev)
{
	int32_t retval = LV0104CS_OK;
	uint8_t mode = LV0104CS_POWER_DOWN;

	if (dev != NULL)
	{
		lv0104cs_get_mode(&mode, dev);
		if(mode != LV0104CS_POWER_ON)
		{
			retval = LV0104CS_E_COMM;
		}
		else
		{
			if (gain == LV0104CS_GAIN_x8
				|| gain == LV0104CS_GAIN_x2
				|| gain == LV0104CS_GAIN_x1
				|| gain == LV0104CS_GAIN_x0p25)
			{
				dev->cmd &= ~LV0104CS_GAIN_MASK;
				dev->cmd |= gain << LV0104CS_GAIN_POSE;
				retval = dev->write_func(dev->id, dev->cmd);
				if (retval != LV0104CS_COMM_OK)
				{
					retval = LV0104CS_E_COMM;
				}
			}
			else
			{
				retval = LV0104CS_E_OUT_OF_RANGE;
			}
		}
	}
	else
	{
		retval = LV0104CS_E_NULL_PTR;
	}
	return retval;
}
/* 
	获取 lv0104cs 增益参数
	LV0104CS_GAIN_x8	8倍
	LV0104CS_GAIN_x2	2倍
	LV0104CS_GAIN_x1	1倍
	LV0104CS_GAIN_x0p25	0.25倍
*/
int32_t lv0104cs_get_gain(uint16_t *gain, struct lv0104cs_t *dev)
{
	int32_t retval = LV0104CS_OK;

	if (dev != NULL && gain != NULL)
	{
		*gain = (dev->cmd & LV0104CS_GAIN_MASK)>>LV0104CS_GAIN_POSE;
	}
	else
	{
		retval = LV0104CS_E_NULL_PTR;
	}
	return retval;
}
/* 
	设置 lv0104cs 灵敏度参数
	sensitivity 范围 0x00-0x3f
	Sensitivity Register :D7     D6     D5     D4     D3     D2     D1     D0
						MODE1  MODE0  ADJ5   ADJ4   ADJ3   ADJ2   ADJ1   ADJ0
	MODE[1:0] :  10       Setting
	ADJ5  :       1       Plus
				  0       Minus
	ADJ[4:0]  :   ADJ[5]=0     2*ADJ[4:0] / (2*ADJ[4:0] + 1)
				  ADJ[5]=1    (2*ADJ[4:0] + 1) / 2*ADJ[4:0]
*/

int32_t lv0104cs_set_sensitivity(uint16_t sensitivity, struct lv0104cs_t *dev)
{
	int32_t retval = LV0104CS_OK;
	uint8_t mode = LV0104CS_POWER_DOWN;

	if (dev != NULL)
	{
		lv0104cs_get_mode(&mode, dev);
		if(mode != LV0104CS_SETTING)
		{
			retval = LV0104CS_E_COMM;
		}
		else
		{
			if (sensitivity <= 0x3F)
			{
				dev->sensitivity = sensitivity;
				retval = dev->write_func(dev->id, dev->sensitivity|0x80);
				if (retval != LV0104CS_COMM_OK)
				{
					retval = LV0104CS_E_COMM;
				}
			}
			else
			{
				retval = LV0104CS_E_OUT_OF_RANGE;
			}
		}
	}
	else
	{
		retval = LV0104CS_E_NULL_PTR;
	}
	return retval;
}
/* 
	获取 lv0104cs 灵敏度参数
	sensitivity 范围 0x00-0x3f
	Sensitivity Register :D7     D6     D5     D4     D3     D2     D1     D0
						MODE1  MODE0  ADJ5   ADJ4   ADJ3   ADJ2   ADJ1   ADJ0
	MODE[1:0] :  10       Setting
	ADJ5  :       1       Plus
				  0       Minus
	ADJ[4:0]  :   ADJ[5]=0     2*ADJ[4:0] / (2*ADJ[4:0] + 1)
				  ADJ[5]=1    (2*ADJ[4:0] + 1) / 2*ADJ[4:0]
*/
int32_t lv0104cs_get_sensitivity(uint16_t *sensitivity, struct lv0104cs_t *dev)
{
	int32_t retval = LV0104CS_OK;

	if (dev != NULL && sensitivity != NULL)
	{
		*sensitivity = dev->sensitivity;
	}
	else
	{
		retval = LV0104CS_E_NULL_PTR;
	}
	return retval;
}

/* 
	读取 lv0104cs 数据
*/
//============================================================================//
//Measuration Result Register(READ) :DH7  DH6  DH5  DH4  DH3  DH2  DH1  DH0         Date Higher Byte
//                                   DL7  DL6  DL5  DL4  DL3  DL2  DL1  DL0         Date Lower Byte
//============================================================================//

int32_t lv0104cs_read_lux_data(uint32_t *lux_data, struct lv0104cs_t *dev)
{
	int32_t retval = LV0104CS_OK;

	uint8_t data[2];

	if (dev != NULL && lux_data != NULL)
	{
		retval = dev->read_func(dev->id, data);
		if (retval == LV0104CS_COMM_OK)
		{
			*lux_data = (((uint16_t)data[0]) << 8) | data[1];
		}
		else
		{
			retval = LV0104CS_E_COMM;
		}
	}
	else
	{
		retval = LV0104CS_E_NULL_PTR;
	}

	return retval;
}


//! \}
