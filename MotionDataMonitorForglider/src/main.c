//-----------------------------------------------------------------------------
// Copyright (c) 2018 Semiconductor Components Industries LLC
// (d/b/a "ON Semiconductor").  All rights reserved.
// This software and/or documentation is licensed by ON Semiconductor under
// limited terms and conditions.  The terms and conditions pertaining to the
// software and/or documentation are available at
// http://www.onsemi.com/site/pdf/ONSEMI_T&C.pdf ("ON Semiconductor Standard
// Terms and Conditions of Sale, Section 8 Software") and if applicable the
// software license agreement.  Do not use this software and/or documentation
// unless you have carefully read and you agree to the limited terms and
// conditions.  By using this software and/or documentation, you agree to the
// limited terms and conditions.
//-----------------------------------------------------------------------------
#include <BDK.h>
#include <BSP_Components.h>
#include <BLE_Components.h>
#include <ansi_color.h>
#include <SEGGER_RTT.h>
#include <rsl10_flash_rom.h>

#include <stdio.h>
#include <stdint.h>

#include <inmp522.h>
#include <lv0104cs.h>
#include <lv0104cs_lux.h>
#include <data_manage2.h>
#include <protocol.h>
#include <HAL_RTC.h>


/* All data written to RX characteristic will be echoed back as a notification
 * send from TX characteristic. */
void BLE_CustomServiceEcho(struct BLE_ICS_RxIndData *ind);
void BLE_CustomRxIndHandler(struct BLE_ICS_RxIndData *ind);

/* Prints message into serial terminal when button is pressed / released. */
void PB_TransitionEvent(void *arg);

void LV0104CS_Callback(uint32_t lux);

/* Copies measurement data for printing of status messages. */
void BME680_Callback(struct BME680_ENV_Data *output);

/* Copies measurement data for printing of status messages. */
void BHY_ActivityCallback(bhy_data_generic_t *data, bhy_virtual_sensor_t sensor);

void BLE_CustomRxIndHandler(struct BLE_ICS_RxIndData *ind);

#define TEMPERATURE_MIN			0
#define TEMPERATURE_MAX			255

#define TEMPERATURE_DM_ADDR		0X00000000
#define TEMPERATURE_DM_LEN		256
#define LIGHT_DM_ADDR			TEMPERATURE_DM_ADDR + TEMPERATURE_DM_LEN
#define LIGHT_DM_LEN			256
#define NOISE_DM_ADDR			LIGHT_DM_ADDR + LIGHT_DM_LEN
#define NOISE_DM_LEN			256
#define FEEDER_DM_ADDR			NOISE_DM_ADDR + NOISE_DM_LEN
#define FEEDER_DM_LEN			10
#define ACTIVITY_DM_ADDR		FEEDER_DM_ADDR + FEEDER_DM_LEN
#define ACTIVITY_DM_LEN			0
#define ROTATION_DM_ADDR		ACTIVITY_DM_ADDR + ACTIVITY_DM_LEN
#define ROTATION_DM_LEN			3072

data_manage_t temperature_dm;
data_manage_t light_dm;
data_manage_t noise_dm;
data_manage_t feeder_dm;
data_manage_t activity_dm;
data_manage_t rotation_dm;

uint16_t round_count;
float sum_gyro;
uint8_t ava_gyro;
uint16_t ava_count = 1;

float angle_front;
float angle_back;

uint8_t temp_buf[256];
uint8_t light_buf[256];
uint8_t noise_buf[256];
uint8_t rotation_buf[3072];

uint32_t time = 0;

int main(void)
{

    /* Initialize BDK library, set system clock (default 8MHz). */
    BDK_InitializeFreq(HAL_CLK_CONF_8MHZ);
    HAL_I2C_SetBusSpeed(HAL_I2C_BUS_SPEED_FAST);
//    HAL_RTC_Initialize();
    /* Initialize all LEDs */
    LED_Initialize(LED_RED);
    LED_Initialize(LED_GREEN);
    LED_Initialize(LED_BLUE);

    LED_On(LED_GREEN);
    HAL_Delay(100);
    LED_Off(LED_GREEN);

    eprom_init();

//    data_manage_init(&temperature_dm,TEMPERATURE_DM_ADDR,TEMPERATURE_DM_LEN);
//    data_manage_init(&light_dm,LIGHT_DM_ADDR,LIGHT_DM_LEN);
//    data_manage_init(&noise_dm,NOISE_DM_ADDR,NOISE_DM_LEN);
//    data_manage_init(&feeder_dm,FEEDER_DM_ADDR,FEEDER_DM_LEN);
//    data_manage_init(&activity_dm,ACTIVITY_DM_ADDR,ACTIVITY_DM_LEN);
//    data_manage_init(&rotation_dm,ROTATION_DM_ADDR,ROTATION_DM_LEN);
	data_manage_init(&temperature_dm,temp_buf,TEMPERATURE_DM_LEN);
	data_manage_init(&light_dm,light_buf,LIGHT_DM_LEN);
	data_manage_init(&noise_dm,noise_buf,NOISE_DM_LEN);
//	data_manage_init(&feeder_dm,FEEDER_DM_ADDR,FEEDER_DM_LEN);
//	data_manage_init(&activity_dm,activity_buf,ACTIVITY_DM_LEN);
	data_manage_init(&rotation_dm,rotation_buf,ROTATION_DM_LEN);
    /* Test LEDs are working */
    LED_On(LED_RED);
    HAL_Delay(100);
    LED_Off(LED_RED);

    /* Initialize BLE stack */
    BDK_BLE_Initialize();
    BDK_BLE_SetLocalName("MDM For glider");
    BLE_ICS_Initialize(BLE_CustomRxIndHandler);

    /* Initialize Button to call callback function when pressed or released. */
    BTN_Initialize(BTN0);
    BTN_Initialize(BTN1);

    /* AttachScheduled -> Callback will be scheduled and called by Kernel Scheduler. */
    /* AttachInt -> Callback will be called directly from interrupt routine. */
    BTN_AttachScheduled(BTN_EVENT_TRANSITION, &PB_TransitionEvent, (void*)BTN0, BTN0);
    BTN_AttachScheduled(BTN_EVENT_TRANSITION, &PB_TransitionEvent, (void*)BTN1, BTN1);

    /** Initialize LV0104CS. */
    LV0104CS_LUX_Initialize();
    LV0104CS_LUX_StartContinuous(1000, LV0104CS_Callback);

    /** Initialize BME680 */
    BME680_ENV_Initialize();
    BME680_ENV_StartPeriodic(BME680_Callback, 1000);

    /** Initialize BHI160 + load BMM150 RAM patch */
    BHI160_NDOF_Initialize();
    BHI160_NDOF_EnableSensor(BHI160_NDOF_S_ACTIVITY_RECOGNITION, BHY_ActivityCallback, 1);

    inmp522_init(1000);

    LED_On(LED_RED);
    HAL_Delay(100);
    LED_Off(LED_RED);
    LED_On(LED_GREEN);
    HAL_Delay(100);
    LED_Off(LED_GREEN);
    LED_On(LED_BLUE);
    HAL_Delay(100);
    LED_Off(LED_BLUE);

    printf("APP: Entering main loop.\r\n");

    while (1)
    {
        /* Execute any events that have occurred & refresh Watchdog timer. */
        BDK_Schedule();
        SYS_WAIT_FOR_INTERRUPT;
    }

    return 0;
}


void BLE_CustomRxIndHandler(struct BLE_ICS_RxIndData *ind)
{
	recv_protocol(ind->data,ind->data_len);
}

void BLE_send(uint8_t *data, uint8_t len)
{
    BLE_ICS_Notify(data, len);
}

void PB_TransitionEvent(void *arg)
{
    ButtonName btn = (ButtonName)arg;

    switch (btn)
    {
    case BTN0:
        LED_Toggle(LED_RED);
        break;
    case BTN1:
        LED_Toggle(LED_GREEN);
        break;
    default:
        return;
    }
//    time = HAL_RTC_GetTime(NULL);
    /* Read current Push Button state. */
    bool button_pressed = BTN_Read(btn);

    /* Print current button state to serial terminal. */
    printf("PB: Button %s state: %s [%lu ms]\r\n", (btn == BTN0) ? "0" : "1",
            (button_pressed == BTN_PRESSED) ? "PRESSED" : "RELEASED",
            HAL_Time());
}


void LV0104CS_Callback(uint32_t lux)
{
	uint8_t lux_data = lux/10;
	data_write(&light_dm,(uint8_t *)&lux_data,1);
}

void BME680_Callback(struct BME680_ENV_Data *output)
{
//    memcpy(&bme680_output, output, sizeof(struct BME680_ENV_Data));
	//温度数据由x100，转换为x10，并且减掉100，温度起点改为10℃，最大温度35.4℃
	int16_t temperature =(output->temperature / 10)-100;
	if(temperature>=UINT8_MAX)
	{
		temperature = UINT8_MAX;
	}
	else if(temperature <= 0 )
	{
		temperature = 0;
	}
	data_write(&temperature_dm,(uint8_t *)&temperature,1);

}

// 角速度信息
void BHY_RateOfRotationCallback(bhy_data_generic_t *data, bhy_virtual_sensor_t sensor)
{
	bhy_data_vector_t bhy_rotation;
//	uint16_t dyn_range = BHI160_NDOF_GetGyroDynamicRange();
    memcpy(&bhy_rotation, &data->data_vector, sizeof(bhy_data_vector_t));
//   / 32768 * 2000 * 60 /360  = 0.0101725f rpm
    float gyro = bhy_rotation.x * 0.0101725f;
    if(gyro >= 0 && gyro > sum_gyro)
    	sum_gyro = gyro;
    else if(gyro < 0 && gyro < -sum_gyro)
    	sum_gyro = -gyro;
    ava_gyro = (uint8_t)(sum_gyro + 127 );
    uint8_t gyro_u8 = gyro + 127;
    data_write(&rotation_dm,(uint8_t *)&gyro_u8,1);
}

// 加速度信息
void BHY_AccelerationCallback(bhy_data_generic_t *data, bhy_virtual_sensor_t sensor)
{
	bhy_data_vector_t bhy_acceleration;
    memcpy(&bhy_acceleration, &data->data_vector, sizeof(bhy_data_vector_t));


}

// 角度信息
void BHY_OrientationCallback(bhy_data_generic_t *data, bhy_virtual_sensor_t sensor)
{
	bhy_data_vector_t bhy_orientation;
    memcpy(&bhy_orientation, &data->data_vector, sizeof(bhy_data_vector_t));
    angle_back = bhy_orientation.x / 32768.0f * 360.0f;
    if((angle_back < 90.f && angle_front >270.f) || (angle_back > 270.f && angle_front < 90.f))
    {
    	round_count++;
    }
    angle_front = angle_back;

}


//当运动状态改变时，启动角速度、角度、加速度数据获取
void BHY_ActivityCallback(bhy_data_generic_t *data, bhy_virtual_sensor_t sensor)
{
	bhy_data_scalar_u16_t bhy_Activity;
	static uint16_t last_bhy_Activity = 0;
	memcpy(&bhy_Activity, &data->data_scalar_u16, sizeof(bhy_data_scalar_u16_t));
	if(bhy_Activity.data == 0x0001 ||bhy_Activity.data == 0x0100)
	{
		if(bhy_Activity.data != last_bhy_Activity){
			switch(bhy_Activity.data)
			{
			case 0x0001:	//still ended
//			    BHI160_NDOF_EnableSensor(BHI160_NDOF_S_LINEAR_ACCELERATION, BHY_AccelerationCallback, 100);
			    BHI160_NDOF_EnableSensor(BHI160_NDOF_S_RATE_OF_ROTATION, BHY_RateOfRotationCallback, 1);
			    BHI160_NDOF_EnableSensor(BHI160_NDOF_S_ORIENTATION, BHY_OrientationCallback, 100);
				break;
			case 0x0100:  //still start

//				BHI160_NDOF_DisableSensor(BHI160_NDOF_S_LINEAR_ACCELERATION);
				BHI160_NDOF_DisableSensor(BHI160_NDOF_S_RATE_OF_ROTATION);
				BHI160_NDOF_DisableSensor(BHI160_NDOF_S_ORIENTATION);
				break;
			}
		}
		last_bhy_Activity = bhy_Activity.data;
	}

}


