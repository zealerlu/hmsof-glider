
#include <BDK.h>
#include <BSP_Components.h>
#include <BLE_Components.h>
#include <ansi_color.h>
#include <SEGGER_RTT.h>

#include "RTE_SoftwareTimer.h"
#include "SoftwareTimer.h"

#include <data_manage2.h>
#include <string.h>
#include <stdlib.h>


void eprom_init(void)
{

}

void data_manage_init(data_manage_t * p_dm,uint8_t* buf,uint32_t length)
{
	memset(buf, 0x00, length);
	p_dm->w_buf = buf;
	p_dm->r_buf = buf;

	p_dm->buf_r_index = 0;
	p_dm->eprom_addr = (uint32_t)buf;
	p_dm->eprom_len = length;

}

void data_read_index_reset(data_manage_t * p_dm)
{
	p_dm->buf_r_index = 0;
}

void data_read(data_manage_t * p_dm,uint16_t cnt,uint8_t * data)
{
	memcpy(data, p_dm->r_buf+(cnt-1)*18 , 18);
//	if(p_dm->buf_r_index < p_dm->eprom_len)
//	{
//		if(p_dm->eprom_len - p_dm->buf_r_index >= 18)
//		{
//			memset(data, 0x00, 18);
//			memcpy(data, p_dm->r_buf , 18);
//			p_dm->buf_r_index += 18;
//		}
//		else
//		{
//			memset(data, 0x00, 18);
//			memcpy(data, p_dm->r_buf , p_dm->eprom_len - p_dm->buf_r_index);
//			p_dm->buf_r_index = p_dm->eprom_len;
//		}
//	}
//	if(p_dm->buf_r_index == p_dm->eprom_len)
//	{
//		p_dm->buf_r_index = 0;
//	}

}

void data_write(data_manage_t * p_dm,uint8_t* data,uint32_t length)
{
	if(p_dm->buf_w_index < p_dm->eprom_len)
	{
		memcpy(p_dm->w_buf + p_dm->buf_w_index ,data , length);
		p_dm->buf_w_index += length;
	}
	else
	{
		p_dm->buf_w_index = 0;
	}
}

uint16_t get_data_pkg_cnt(data_manage_t * p_dm)
{
	uint16_t cnt = 0;
	if(p_dm->buf_w_index % 18 != 0)
	{
		cnt = p_dm->buf_w_index / 18 + 1;
	}
	else
	{
		cnt = p_dm->buf_w_index / 18;
	}
	return cnt;
}

void data_empty(data_manage_t * p_dm)
{
	p_dm->buf_w_index = 0;
	p_dm->buf_w_num = 0;

	memset(p_dm->w_buf,0x00,p_dm->eprom_len);

}
