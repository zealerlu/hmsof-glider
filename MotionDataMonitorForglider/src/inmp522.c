#include <BDK.h>
#include <BSP_Components.h>
#include <BLE_Components.h>
#include <ansi_color.h>
#include <SEGGER_RTT.h>
#include <math.h>
#include <SoftwareTimer.h>

#include <data_manage2.h>

#define AUDIO_DMIC0_GAIN                0x800
#define AUDIO_DMIC1_GAIN                0x800
#define AUDIO_OD_GAIN                   0x800

#define AUDIO_CONFIG                    (OD_AUDIOSLOWCLK            | \
                                         DMIC_AUDIOCLK              | \
                                         DECIMATE_BY_64             | \
                                         OD_UNDERRUN_PROTECT_ENABLE | \
                                         OD_DATA_MSB_ALIGNED        | \
                                         DMIC0_DATA_LSB_ALIGNED     | \
                                         DMIC1_DATA_LSB_ALIGNED     | \
                                         OD_DMA_REQ_DISABLE         | \
                                         DMIC0_DMA_REQ_DISABLE      | \
                                         DMIC1_DMA_REQ_DISABLE      | \
                                         OD_INT_GEN_DISABLE         | \
                                         DMIC0_INT_GEN_ENABLE       | \
                                         DMIC1_INT_GEN_DISABLE      | \
                                         OD_DISABLE                 | \
                                         DMIC0_ENABLE               | \
                                         DMIC1_DISABLE)
#define DMIC_BUF_SIZE 					 10


static const float kMinLevel = 30.f;
float _db = 0.f;
float _db_max = 0.f;
static int16_t dmic_value[DMIC_BUF_SIZE] = {0};
static int16_t dmic_index = 0;
static struct SwTimer inmp522_timer;
static void INMP522_ContinuousTimerCallback(void *arg);

float db_value;

void inmp522_init(uint32_t period_ms)
{
    /** Configure DMIC input to test INMP522 microphone. */

    /* Configure AUDIOCLK to 2 MHz and AUDIOSLOWCLK to 1 MHz. */
    CLK->DIV_CFG1 &= ~(AUDIOCLK_PRESCALE_64 | AUDIOSLOWCLK_PRESCALE_4);
    CLK->DIV_CFG1 |= AUDIOCLK_PRESCALE_4 | AUDIOSLOWCLK_PRESCALE_2;

    /* Configure OD, DMIC0 and DMIC1 */
    Sys_Audio_Set_Config(AUDIO_CONFIG);

    Sys_Audio_Set_DMICConfig(DMIC0_DCRM_CUTOFF_20HZ | DMIC1_DCRM_CUTOFF_20HZ |
                             DMIC1_DELAY_DISABLE | DMIC0_FALLING_EDGE |
                             DMIC1_RISING_EDGE, 0);

    Sys_Audio_DMICDIOConfig(DIO_6X_DRIVE | DIO_LPF_DISABLE | DIO_NO_PULL,
                            10, 6, DIO_MODE_AUDIOCLK);

    /* Configure Gains for DMIC0, DMIC1 and OD */
    AUDIO->DMIC0_GAIN = AUDIO_DMIC0_GAIN;
    NVIC_EnableIRQ(DMIC_OUT_OD_IN_IRQn);


	SwTimer_Initialize(&inmp522_timer);
	SwTimer_AttachScheduled(&inmp522_timer,&INMP522_ContinuousTimerCallback, NULL);
	SwTimer_ExpireInMs(&inmp522_timer, period_ms);


}

static float inmp522_process(void)
{
    float sum_square_ = 0;
    for (size_t i = 0; i < DMIC_BUF_SIZE; ++i) {
        sum_square_ += abs(dmic_value[i]);
    }
    float rms = sum_square_ / (DMIC_BUF_SIZE);

    rms = 20.f * log10(rms);
    if (rms < kMinLevel)
        rms = kMinLevel;
    return rms;
}


/*
    连续读取定时器回调函数
*/
static void INMP522_ContinuousTimerCallback(void *arg)
{
	extern data_manage_t noise_dm;

    SwTimer_Advance(&inmp522_timer);

    db_value = (float)((_db_max-30.f)*4.f);
    uint8_t data = (uint8_t)db_value;
    _db_max = 0;
    data_write(&noise_dm,(uint8_t *)&data,1);
}


void DMIC_OUT_OD_IN_IRQHandler(void)
{

	if(dmic_index >= DMIC_BUF_SIZE){
		_db = inmp522_process();
		if(_db > _db_max){
			_db_max = _db;
		}
		dmic_index = 0;
	}
	dmic_value[dmic_index] = (int16_t)AUDIO->DMIC0_DATA;
	dmic_index++;

}
