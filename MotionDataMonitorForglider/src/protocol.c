
#include <data_manage2.h>
#include <protocol.h>

extern uint16_t round_count;
extern float angle_front;
extern float angle_back;
extern uint8_t ava_gyro;

extern data_manage_t temperature_dm;
extern data_manage_t light_dm;
extern data_manage_t noise_dm;
extern data_manage_t feeder_dm;
extern data_manage_t activity_dm;
extern data_manage_t rotation_dm;
uint8_t recv_protocol(uint8_t * data, uint8_t length)
{
	uint8_t send_data[20] = {0};
	uint16_t cnt = 0;
	if(length >= 2)
	{
		switch(data[0])
		{
			case CMD_CLEAN_ROUND:
				round_count = 0;
				angle_front = 0;
				angle_back = 0;
				ava_gyro = 0;
				send_data[0] = CMD_CLEAN_ROUND;
				send_data[1] = 0x01;
				send_data[2] = 0x11;
				BLE_send(send_data, 3);
				break;
			case CMD_CLEAN_BUFF:
				data_empty(&temperature_dm);
				data_empty(&light_dm);
				data_empty(&noise_dm);
				data_empty(&feeder_dm);
				data_empty(&activity_dm);
				data_empty(&rotation_dm);
				send_data[0] = CMD_CLEAN_BUFF;
				send_data[1] = 0x01;
				send_data[2] = 0x11;
				BLE_send(send_data, 3);
				break;
			case CMD_SET_TIME:
				send_data[0] = CMD_SET_TIME;
				send_data[1] = 0x01;
				send_data[2] = 0x11;
				BLE_send(send_data, 3);
				break;
			case CMD_SET_FEETER:
				send_data[0] = CMD_SET_FEETER;
				send_data[1] = 0x01;
				send_data[2] = 0x11;
				BLE_send(send_data, 3);
				break;
			case CMD_GET_COUND_AVAGYRO:
				send_data[0] = CMD_GET_COUND_AVAGYRO;
				send_data[1] = 0x03;
				send_data[2] = round_count >> 8 & 0xff;
				send_data[3] = round_count & 0xff;
				send_data[4] = (int8_t)ava_gyro;
				BLE_send(send_data, 5);
				break;

			case CMD_GET_TEMP_PKG_CNT:
				cnt = get_data_pkg_cnt(&temperature_dm);
				send_data[0] = CMD_GET_TEMP_PKG_CNT;
				send_data[1] = 0x02;
				send_data[2] = cnt >> 8 & 0xff;
				send_data[3] = cnt & 0xff;
				BLE_send(send_data, 4);
				break;

			case CMD_GET_TEMP_PKG:
				cnt = data[2] << 8 | data[3];
				send_data[0] = CMD_GET_TEMP_PKG;
				send_data[1] = 0x14;
				data_read(&temperature_dm,cnt,&send_data[2]);
				BLE_send(send_data, 20);
				break;

			case CMD_GET_LIGHT_PKG_CNT:
				cnt = get_data_pkg_cnt(&light_dm);
				send_data[0] = CMD_GET_LIGHT_PKG_CNT;
				send_data[1] = 0x02;
				send_data[2] = cnt >> 8 & 0xff;
				send_data[3] = cnt & 0xff;
				BLE_send(send_data, 4);
				break;

			case CMD_GET_LIGHT_PKG:
				cnt = data[2] << 8 | data[3];
				send_data[0] = CMD_GET_LIGHT_PKG;
				send_data[1] = 0x14;
				data_read(&light_dm,cnt,&send_data[2]);
				BLE_send(send_data, 20);
				break;
			case CMD_GET_NOISE_PKG_CNT:
				cnt = get_data_pkg_cnt(&noise_dm);
				send_data[0] = CMD_GET_NOISE_PKG_CNT;
				send_data[1] = 0x02;
				send_data[2] = cnt >> 8 & 0xff;
				send_data[3] = cnt & 0xff;
				BLE_send(send_data, 4);
				break;

			case CMD_GET_NOISE_PKG:
				cnt = data[2] << 8 | data[3];
				send_data[0] = CMD_GET_NOISE_PKG;
				send_data[1] = 0x14;
				data_read(&noise_dm,cnt,&send_data[2]);
				BLE_send(send_data, 20);
				break;

			case CMD_GET_ACT_PKG_CNT:
				cnt = get_data_pkg_cnt(&rotation_dm);
				send_data[0] = CMD_GET_ACT_PKG_CNT;
				send_data[1] = 0x02;
				send_data[2] = cnt >> 8 & 0xff;
				send_data[3] = cnt & 0xff;
				BLE_send(send_data, 4);
				break;

			case CMD_GET_ACT_PKG:
				cnt = data[2] << 8 | data[3];
				send_data[0] = CMD_GET_ACT_PKG;
				send_data[1] = 0x14;
				data_read(&rotation_dm,cnt,&send_data[2]);
				BLE_send(send_data, 20);
				break;
		}
	}
	return 0 ;
}
