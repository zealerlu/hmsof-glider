// pages/Debug.js

const FS_deviceid = "D5:BB:FF:22:11:94"
const AR_deviceid = "11:FF:FF:22:11:94"

const serviceid = "E093F3B5-00A3-A9E5-9ECA-40016E0EDC24"
const fs_write_charaid = "E093F3B5-00A3-A9E5-9ECA-40026E0EDC24"

const ar_read_notifyid = "E093F3B6-00A3-A9E5-9ECA-40026E0EDC24"
const ar_write_charaid = "E093F3B7-00A3-A9E5-9ECA-40036E0EDC24"

function inArray(arr, key, val) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i][key] === val) {
      return i;
    }
  }
  return -1;
}

// ArrayBuffer转16进度字符串示例
function ab2hex(buffer) {
  var hexArr = Array.prototype.map.call(
    new Uint8Array(buffer),
    function (bit) {
      return ('00' + bit.toString(16)).slice(-2)
    }
  )
  return hexArr.join('');
}

Page({

  /**
   * 页面的初始数据
   */
  data: {
    ar_connect_bt_name: "连接",
    ar_connect_text_name: "",
    ar_connect_status: false,
    ar_connect_bt_disabled: true,
    ar_connect_dev_name: "",

    rounds_value: "",
    aspeed_value: "",
    temp_pkg_counts: "",
    light_pkg_counts: "",
    noise_pkg_counts: "",
    action_pkg_counts: "",

    fs_connect_bt_name: "连接",
    fs_connect_text_name: "",
    fs_connect_status: false,
    fs_connect_bt_disabled: true,
    fs_connect_dev_name: "",

    ar_recv_data: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.openBluetoothAdapter()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    this.stopBluetoothDevicesDiscovery()
    wx.closeBluetoothAdapter({
      success: (res) => {
        console.log('closeBluetoothAdapter success', res)
      },
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    this.stopBluetoothDevicesDiscovery()
    wx.closeBluetoothAdapter({
      success: (res) => {
        console.log('closeBluetoothAdapter success', res)
      },
    })
  },

   // 开始扫描，打开蓝牙适配器
  openBluetoothAdapter() {
    this.setData({
      ar_connect_text_name: "开始扫描",
      fs_connect_text_name: "开始扫描",
    })
    wx.openBluetoothAdapter({
      success: (res) => {
        console.log('openBluetoothAdapter success', res)
        this.startBluetoothDevicesDiscovery()
      },
      fail: (res) => {
        if (res.errCode === 10001) {
          wx.onBluetoothAdapterStateChange(function (res) {
            console.log('onBluetoothAdapterStateChange', res)
            if (res.available) {
              this.startBluetoothDevicesDiscovery()
            }
          })
        }
      }
    })
  },

  closeBluetoothAdapter() {
    wx.closeBluetoothAdapter()
  },

// 开始搜索设备
  startBluetoothDevicesDiscovery() {
    wx.startBluetoothDevicesDiscovery({
      allowDuplicatesKey: true,
      success: (res) => {

        console.log('startBluetoothDevicesDiscovery success', res)
        this.onBluetoothDeviceFound()
      },
    })
  },

  stopBluetoothDevicesDiscovery() {
    wx.stopBluetoothDevicesDiscovery()
  },

  onBluetoothDeviceFound() {
    wx.onBluetoothDeviceFound((res) => {
      res.devices.forEach(device => {
        if (!device.name && !device.localName) {
          return
        }
        if(device.deviceId == FS_deviceid){
          console.log('found 奖励器 ',device)
          this.setData({
            fs_connect_dev_name: device.name,
            fs_connect_text_name: "搜索到奖励器",
            fs_connect_bt_disabled: false,
          })
        }
        if(device.deviceId == AR_deviceid){
          console.log('found 监测器 ',device)
          this.setData({
            ar_connect_dev_name: device.name,
            ar_connect_text_name: "搜索到监测器",
            ar_connect_bt_disabled: false,
          })
        }
        if(this.data.fs_connect_bt_disabled == false && this.data.ar_connect_bt_disabled == false){
          this.stopBluetoothDevicesDiscovery()
        }
      })

    })
  },

  createARConnection() {
    if(this.data.ar_connect_status == false){
      wx.createBLEConnection({
        deviceId: AR_deviceid,
        success: (res) => {
          console.log('create AR connection success ',res)
          this.setData({
            ar_connect_bt_name: "断开",
            ar_connect_text_name: "连接到：" + this.data.ar_connect_dev_name,
            ar_connect_status: true,
          })
          this.getBLEDeviceServices(AR_deviceid)
        },
        fail: (res) => {
          console.log('createBLEConnection fail ',res)
        }
      })
      
    }
    else{
      wx.closeBLEConnection({
        deviceId: AR_deviceid,
        success: (res) => {
          console.log('close AR connection success ',res)
          this.setData({
            ar_connect_bt_name: "连接",
            ar_connect_status: false,
            ar_connect_bt_disabled: true,
            ar_connect_text_name: "扫描中...",
            rounds_value: "",
            aspeed_value: "",
            temp_pkg_counts: "",
            light_pkg_counts: "",
            noise_pkg_counts: "",
            action_pkg_counts: "",
          })
        },
        fail: (res) => {
          console.log('close AR connection fail ',res)
        }
      })
      this.startBluetoothDevicesDiscovery()
    }
  },

  createFSConnection() {
    if(this.data.fs_connect_status == false){
      wx.createBLEConnection({
        deviceId: FS_deviceid,
        success: (res) => {
          console.log('createBLEConnection success ',res)
          this.setData({
            fs_connect_bt_name: "断开",
            fs_connect_text_name: "连接到：" + this.data.fs_connect_dev_name,
            fs_connect_status: true,
          })
          this.getBLEDeviceServices(FS_deviceid)
        },
        fail: (res) => {
          console.log('createBLEConnection fail ',res)
        }
      })
    }else{
      wx.closeBLEConnection({
        deviceId: FS_deviceid,
        success: (res) => {
          console.log('close FS connection success ',res)
          this.setData({
            fs_connect_bt_name: "连接",
            fs_connect_text_name: "",
            fs_connect_status: false,
            fs_connect_bt_disabled: true,
            fs_connect_text_name: "扫描中...",
          })
        },
        fail: (res) => {
          console.log('close FS connection fail ',res)
        }
      })
      this.startBluetoothDevicesDiscovery()
    }
  },

  getBLEDeviceServices(deviceid) {
    wx.getBLEDeviceServices({
      deviceId :deviceid,
      success: (res) => {
        console.log(res.services.length)
        for (let i = 0; i < res.services.length; i++) {
          console.log(res.services[i].uuid)
          if(res.services[i].uuid== serviceid)
          {
            this.getBLEDeviceCharacteristics(deviceid, res.services[i].uuid)
            return
          }
        }
      }
    })
  },

  getBLEDeviceCharacteristics(deviceid, serviceid) {
    wx.getBLEDeviceCharacteristics({
      deviceId :deviceid,
      serviceId:serviceid,
      success: (res) => {
        console.log('getBLEDeviceCharacteristics success', res.characteristics)
        for (let i = 0; i < res.characteristics.length; i++) {
          let item = res.characteristics[i]
          if (deviceid == AR_deviceid && item.uuid == ar_read_notifyid){
            console.log('notify ar charactoristic value success')
            wx.notifyBLECharacteristicValueChange({
              deviceId :deviceid,
              serviceId :serviceid,
              characteristicId: item.uuid,
              state: true,
            })
          }
        }
      },
      fail(res) {
        console.error('getBLEDeviceCharacteristics', res)
      }
    })
    // 操作之前先监听，保证第一时间获取数据
    wx.onBLECharacteristicValueChange((res) => {
      console.log("onBLECharacteristicValueChange",res)
      this.processARprotobuf(res.value)
    })
  },

  writeBLECharacteristicValue(dev,charid,buf) {
    wx.writeBLECharacteristicValue({
      deviceId: dev,
      serviceId: serviceid,
      characteristicId: charid,
      value: buf,
      success: (res) =>{
        console.log('writeBLECharacteristicValue', res)
      },
      fail: (res) => {
        console.log('writeBLECharacteristicValue', res)
        if (res.errCode === 10006) {
          wx.onBluetoothAdapterStateChange(function (res) {
            console.log('writeBLECharacteristicValue', res)
            if (res.available) {
              this.startBluetoothDevicesDiscovery()
            }
          })
        }
      }
    })
  },
  
  setFSopen(num){
    wx.showToast({
      title: '设置'+num+"打开成功",
      icon: 'success',
      duration: 1000
    })
    let buffer = new ArrayBuffer(2)
    let dataView = new DataView(buffer)
    dataView.setUint8(0, 0xff)
    dataView.setUint8(1, 0x01 << (num - 1))
    console.log("set data",buffer)
    this.writeBLECharacteristicValue(FS_deviceid,fs_write_charaid,buffer)
  },

  setFSNum1() {
    this.setFSopen(1)
  },

  setFSNum2() {
    this.setFSopen(2)
  },

  setFSNum3() {
    this.setFSopen(3)
  },

  setFSNum4() {
    this.setFSopen(4)
  },

  setFSNum5() {
    this.setFSopen(5)
  },

  processARprotobuf(res){
    
    let data = new Uint8Array(res)

    switch(data[0]){
      case 0x80 :
        if(data[2] == 0x11)
          this.setData({
            ar_connect_text_name: "get！٩( 'ω' )و "
          })
      break
      case 0x81:
        if(data[2] == 0x11)
        this.setData({
          ar_connect_text_name: "get！٩( 'ω' )و "
        })
      break
      case 0x82:
        this.setData({
          ar_connect_text_name: "get！٩( 'ω' )و "
        })
      break
      case 0x10:
        this.setData({
          ar_connect_text_name: "get！٩( 'ω' )و ",
          rounds_value: (data[2] << 8 | data[3]),
          aspeed_value: data[4] == 0? 0 : data[4] >= 127? (data[4]-127):(127-data[4])
        })
      break
      
      case 0x11:
        this.setData({
          ar_connect_text_name: "get！٩( 'ω' )و ",
          temp_pkg_counts: (data[2] << 8 | data[3]),
        })
      break
      case 0x12:
        this.setData({
          ar_connect_text_name: "get！٩( 'ω' )و ",
          light_pkg_counts: (data[2] << 8 | data[3]),
        })
      break

      case 0x13:
        this.setData({
          ar_connect_text_name: "get！٩( 'ω' )و ",
          noise_pkg_counts: (data[2] << 8 | data[3]),
        })
      break
      case 0x14:
        this.setData({
          ar_connect_text_name: "get！٩( 'ω' )و ",
          action_pkg_counts: (data[2] << 8 | data[3]),
        })
      break
    }
  },

  cleanRoundCtrl(){
    this.setData({
      ar_connect_text_name: "（づ￣3￣）づ╭❤～"
    })
    wx.showToast({
      title: "清除总圈数",
      icon: 'success',
      duration: 1000
    })
    console.log("cleanRoundCtrl",buffer)
    let buffer = new ArrayBuffer(2)
    let dataView = new DataView(buffer)
    dataView.setUint8(0, 0x80)
    dataView.setUint8(1, 0x00)
    this.writeBLECharacteristicValue(AR_deviceid,ar_write_charaid,buffer)
  },

  cleanBufferCtrl(){
    this.setData({
      ar_connect_text_name: "（づ￣3￣）づ╭❤～"
    })
    wx.showToast({
      title: "清除缓冲区",
      icon: 'success',
      duration: 1000
    })
    let buffer = new ArrayBuffer(2)
    let dataView = new DataView(buffer)
    dataView.setUint8(0, 0x81)
    dataView.setUint8(1, 0x00)
    console.log("cleanBufferCtrl",buffer)
    this.writeBLECharacteristicValue(AR_deviceid,ar_write_charaid,buffer)
  },

  setTimeCtrl(){
    this.setData({
      ar_connect_text_name: "（づ￣3￣）づ╭❤～"
    })
    wx.showToast({
      title: "设置时间",
      icon: 'success',
      duration: 1000
    })
    let buffer = new ArrayBuffer(4)
    let dataView = new DataView(buffer)
    dataView.setUint8(0, 0x81)
    dataView.setUint8(1, 0x02)
    dataView.setUint8(2, 0x00)
    dataView.setUint8(3, 0x00)
    console.log("setTimeCtrl",buffer)
    this.writeBLECharacteristicValue(AR_deviceid,ar_write_charaid,buffer)
  },

  getRoundCtrl(){
    this.setData({
      ar_connect_text_name: "（づ￣3￣）づ╭❤～"
    })
    wx.showToast({
      title: "获取圈数",
      icon: 'success',
      duration: 1000
    })
    let buffer = new ArrayBuffer(2)
    let dataView = new DataView(buffer)
    dataView.setUint8(0, 0x10)
    dataView.setUint8(1, 0x00)
    console.log("getRoundCtrl",buffer)
    this.writeBLECharacteristicValue(AR_deviceid,ar_write_charaid,buffer)
  },
  getTempPkgsCounstsCtrl(){
    this.setData({
      ar_connect_text_name: "（づ￣3￣）づ╭❤～"
    })
    wx.showToast({
      title: "获取温度包数",
      icon: 'success',
      duration: 1000
    })
    let buffer = new ArrayBuffer(2)
    let dataView = new DataView(buffer)
    dataView.setUint8(0, 0x11)
    dataView.setUint8(1, 0x00)
    console.log("getTempPkgsCounstsCtrl",buffer)
    this.writeBLECharacteristicValue(AR_deviceid,ar_write_charaid,buffer)
  },
  getLightPkgsCounstsCtrl(){
    this.setData({
      ar_connect_text_name: "（づ￣3￣）づ╭❤～"
    })
    wx.showToast({
      title: "获取亮度包数",
      icon: 'success',
      duration: 1000
    })
    let buffer = new ArrayBuffer(2)
    let dataView = new DataView(buffer)
    dataView.setUint8(0, 0x12)
    dataView.setUint8(1, 0x00)
    console.log("getLightPkgsCounstsCtrl",buffer)
    this.writeBLECharacteristicValue(AR_deviceid,ar_write_charaid,buffer)
  },
  getNoisePkgsCounstsCtrl(){
    this.setData({
      ar_connect_text_name: "（づ￣3￣）づ╭❤～"
    })
    wx.showToast({
      title: "获取噪声包数",
      icon: 'success',
      duration: 1000
    })
    let buffer = new ArrayBuffer(2)
    let dataView = new DataView(buffer)
    dataView.setUint8(0, 0x13)
    dataView.setUint8(1, 0x00)
    console.log("getNoisePkgsCounstsCtrl",buffer)
    this.writeBLECharacteristicValue(AR_deviceid,ar_write_charaid,buffer)
  },
  getActionPkgsCounstsCtrl(){
    this.setData({
      ar_connect_text_name: "（づ￣3￣）づ╭❤～"
    })
    wx.showToast({
      title: "获取运动包数",
      icon: 'success',
      duration: 1000
    })
    let buffer = new ArrayBuffer(2)
    let dataView = new DataView(buffer)
    dataView.setUint8(0, 0x14)
    dataView.setUint8(1, 0x00)
    console.log("getActionPkgsCounstsCtrl",buffer)
    this.writeBLECharacteristicValue(AR_deviceid,ar_write_charaid,buffer)
  },



})